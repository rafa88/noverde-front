export async function requestLoan(data) {
  return fetch("https://api.noverde-hmg.net/fakes", {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      "Authorization": "Bearer e6f710ecca5b476189c7ecdde4392cc8",
      "Accept": "application/json",
      "Content-Type": "application/json",
    },
  }).then((res) => res.json());
}
