export function InputMaskMoney(value) {
  const v = ((value.replace(/\D/g, "") / 100).toFixed(2) + "").split(".");

  const m = v[0]
    .split("")
    .reverse()
    .join("")
    .match(/.{1,3}/g);

  for (let i = 0; i < m.length; i++)
    m[i] = m[i].split("").reverse().join("") + ".";

  const r = m.reverse().join("");

  return r.substring(0, r.lastIndexOf(".")) + "," + v[1];
}

export function convertToUSD(value) {
  return value.replace(".", "").replace(",", ".");
}

export function maskPrice(value) {
  const newValue = parseFloat(value).toFixed(2).split(".");
  newValue[0] = newValue[0].split(/(?=(?:...)*$)/).join(".");
  return newValue.join(",");
}

export function maskDate(value) {
  return `${value.substr(8)}/${value.substr(5, 2)}/${value.substr(0, 4)}`;
}

export function maskCpf(cpf) {
  cpf = cpf.replace(/\D/g, "");
  cpf = cpf.replace(/(\d{3})(\d)/, "$1.$2");
  cpf = cpf.replace(/(\d{3})(\d)/, "$1.$2");
  cpf = cpf.replace(/(\d{3})(\d{1,2})$/, "$1-$2");
  return cpf;
}
