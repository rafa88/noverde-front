import { combineReducers } from "redux";

import { loansReducer } from "./LoansReducer";

export const Reducers = combineReducers({
  loans: loansReducer
});
