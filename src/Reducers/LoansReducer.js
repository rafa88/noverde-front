const obj = JSON.parse(localStorage.getItem("storage")) || {};
const initialState = {
  cpf: obj.cpf,
  amount: obj.amount,
  first_due_date: obj.first_due_date,
  installment: obj.installment,
  period: obj.period,
  requestedAmount: obj.requestedAmount,
  status: obj.status,
  loading: false
};

export const loansReducer = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_CPF":
      return {
        ...state,
        cpf: action.value,
      };

    case "FETCH_LOAN":
      return {
        ...state,
        requestedAmount: action.value.requestedAmount,
        loading: true,
      };

    case "RECEIVE_LOAN":
      return {
        ...state,
        amount: action.value.amount,
        first_due_date: action.value.first_due_date,
        installment: action.value.installment,
        period: action.value.period,
        status: action.value.status,
        loading: false,
      };

    case "RECEIVE_LOAN_ERROR":
      return {
        ...state,
        status: "denied",
        loading: false,
      };

    default:
      return state;
  }
};
