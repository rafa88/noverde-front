import { requestLoan } from "../Services/Services";

export const addCpf = (cpf) => (dispatch, getState) => {
  localStorage.setItem("storage", JSON.stringify({ cpf }));

  dispatch({ type: "ADD_CPF", value: cpf });
};

export const addPrice = (cpf) => (dispatch, getState) => {
  localStorage.setItem("storage", JSON.stringify({ cpf }));

  dispatch({ type: "ADD_CPF", value: cpf });
};

export const getLoan = (data, requestedAmount, hooks = {}) => async (dispatch, getState) => {
  const { cpf } = getState().loans;
  dispatch({ type: "FETCH_LOAN", value: requestedAmount });

  requestLoan(data)
    .then((result) => {
      let object = { cpf, requestedAmount };
      if (result.status === "approved") {
        object = { ...result, ...object };
        dispatch({ type: "RECEIVE_LOAN", value: result });
      } else {
        object = { status: "denied", ...object };
        dispatch({ type: "RECEIVE_LOAN_ERROR" });
      }
      localStorage.setItem("storage", JSON.stringify(object));
      hooks.onSuccess();
    })
    .catch((error) => {
      dispatch({ type: "RECEIVE_LOAN_ERROR" });
    });
};
