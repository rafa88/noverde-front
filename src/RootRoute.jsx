import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import Home from "./Component/Home/Home";
import Amount from "./Component/Amount/Amount";
import Result from "./Component/Result/Result";

export const URL_HOME = `/`;
export const URL_AMOUNT = `/seu-valor`;
export const URL_RESULT = `/resultado`;

const RootRoute = () => (
  <BrowserRouter>
    <Switch>
      <Route path={URL_HOME} exact={true} component={Home} />
      <Route path={URL_AMOUNT} exact={true} component={Amount} />
      <Route path={URL_RESULT} exact={true} component={Result} />
    </Switch>
  </BrowserRouter>
);

export default RootRoute;
