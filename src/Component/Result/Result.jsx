import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { maskDate, maskPrice } from "../../Helpers/Mask";
import Steps from "../Steps/Steps";

import "./Result.scss";

class Result extends React.Component {
  render() {
    const {
      amount,
      first_due_date,
      installment,
      period,
      requestedAmount,
      status,
    } = this.props;

    return (
      <div>
        <Steps
          current={3}
          color={status === "approved" ? "green" : "grey"}
        />

        <section
          className={`section-container result-container ${
            status === "approved" ? "bg-green" : "bg-dark-grey"
          }`}
        >
          {status === "approved" ? (
            <div>
              <div className="result-message">
                <h1 className="result-message-title">
                  Uhull,
                  <br className="hidedesktop" /> você foi aprovado!
                </h1>
                {amount < requestedAmount && (
                  <p className="result-message-subtitle hidemobile">
                    *O valor que liberamos é menor do que o escolhido
                  </p>
                )}
              </div>
              <section className="result-details">
                <p className="result-item">
                  Limite liberado
                  <br />
                  <strong>
                    <small>R$</small> {maskPrice(amount)}
                  </strong>
                </p>
                <p className="result-item">
                  Parcelas para pagar
                  <br />
                  <strong>
                    {period}x {maskPrice(installment)}
                  </strong>
                </p>
                <p className="result-item">
                  Primeiro Vencimento
                  <br />
                  <strong>{maskDate(first_due_date)}</strong>
                </p>
                {amount < requestedAmount && (
                  <p className="result-lower-value hidedesktop">
                    *O valor que liberado é menor que o<br /> valor escolhido
                  </p>
                )}
              </section>
            </div>
          ) : (
            <div>
              <div className="result-message">
                <h1 className="result-message-title">
                  Não conseguimos
                  <br className="hidedesktop" /> aprovar o seu
                  <br className="hidemobile" /> empréstimo :(
                </h1>
              </div>
            </div>
          )}
        </section>
      </div>
    );
  }
}

export default withRouter(
  connect((state) => {
    return {
      amount: state.loans.amount,
      first_due_date: state.loans.first_due_date,
      installment: state.loans.installment,
      period: state.loans.period,
      requestedAmount: state.loans.requestedAmount,
      status: state.loans.status,
    };
  }, null)(Result)
);
