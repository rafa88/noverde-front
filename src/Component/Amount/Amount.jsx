import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import { InputMaskMoney, convertToUSD } from "../../Helpers/Mask";
import { validateValue } from "../../Helpers/Validation";
import { URL_RESULT } from "../../RootRoute";
import Loading from "../Loading/Loading";
import Steps from "../Steps/Steps";
import { addPrice, getLoan } from "../../Actions/LoansActions";

class Amount extends React.Component {
  constructor(props) {
    super(props);
    const { requestedAmount } = props;

    this.state = {
      requestedAmount: requestedAmount ? InputMaskMoney(requestedAmount) : ""
    };
  }

  sendForm = (e) => {
    e.preventDefault();
    const { history, getLoan, cpf } = this.props;
    console.log(cpf);
    const requestedAmount = convertToUSD(this.state.requestedAmount);
    const data = { cpf, amount: Number(requestedAmount) };

    getLoan(data, requestedAmount, {
      onSuccess: () => {
        history.push(URL_RESULT);
      },
    });
  };

  handleChange = (e) => {
    this.setState({ requestedAmount: InputMaskMoney(e.target.value) });
  };

  render() {
    const { requestedAmount } = this.state;
    const { loading } = this.props;
    return (
      <div>
        {loading && <Loading />}
        <Steps current={2} />

        <section className="section-container bg-grey">
          <section className="section-content">
            <header className="section-title">
              <p className="title">
                De quanto você precisa pra ficar
                <br className="hidedesktop" /> #NOVERDE
              </p>
            </header>
            <form className="form" onSubmit={this.sendForm}>
              <div className="form-group">
                <div className="form-input">
                  <input
                    placeholder="R$"
                    value={requestedAmount}
                    onChange={this.handleChange}
                  />
                </div>
                <div className="form-button">
                  <button
                    className="btn btn-green"
                    disabled={!validateValue(requestedAmount)}
                  >
                    Próximo
                  </button>
                </div>
              </div>
            </form>
          </section>
        </section>
      </div>
    );
  }
}

export default withRouter(
  connect(
    (state) => {
      return {
        cpf: state.loans.cpf,
        loading: state.loans.loading,
      };
    },
    (dispatch) => {
      return {
        addPrice: (object) => {
          dispatch(addPrice(object));
        },
        getLoan: (data, requestedAmount, hooks) =>
          dispatch(getLoan(data, requestedAmount, hooks)),
      };
    }
  )(Amount)
);
