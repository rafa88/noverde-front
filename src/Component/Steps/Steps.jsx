import React from "react";
import "./Steps.scss";

function Steps({ current, color = "green" }) {
  return (
    <header className="header">
      <div className={`steps steps-${color}`}>
        <div
          className={`steps-item ${
            current > 1 ? "steps-item-finish" : "steps-item-process"
          }`}
        >
          <div className="steps-item-tail"></div>
          <div className="steps-item-number">Passo 1</div>
          <div className="steps-item-content">CPF</div>
        </div>
        <div
          className={`steps-item ${
            current > 2
              ? "steps-item-finish"
              : current === 2 && "steps-item-process"
          }`}
        >
          <div className="steps-item-tail"></div>
          <div className="steps-item-number">Passo 2</div>
          <div className="steps-item-content">Crédito</div>
        </div>
        <div
          className={`steps-item ${current === 3 ? "steps-item-process" : ""}`}
        >
          <div className="steps-item-content">Resultado</div>
        </div>
      </div>
    </header>
  );
}

export default Steps;
