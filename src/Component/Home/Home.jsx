import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import { validateCpf } from "../../Helpers/Validation";
import { addCpf } from "../../Actions/LoansActions";
import { maskCpf } from "../../Helpers/Mask";
import { URL_AMOUNT } from "../../RootRoute";
import Steps from "../Steps/Steps";

class Home extends React.Component {
  constructor(props) {
    super(props);
    const { cpf } = props;

    this.state = {
      cpf: cpf ? maskCpf(cpf) : "",
      errors: {},
    };
  }

  sendForm = (e) => {
    e.preventDefault();

    const cpf = this.state.cpf.replace(/\D/g, "");
    this.props.addCpf(cpf)
    this.props.history.push(URL_AMOUNT);
  };

  handleChange = (e) => {
    const cpf = maskCpf(e.target.value);
    this.setState({ cpf });
  };

  render() {
    const { cpf } = this.state;
    return (
      <div>
        <Steps current={1} />

        <section className="section-container bg-grey">
          <section className="section-content">
            <header className="section-title">
              <p className="title">
                Olá, queremos te ver #NOVERDE, para
                <br className="hidedesktop" /> isso precisamos
                <br /> do seu CPF.
              </p>
            </header>
            <form className="form" onSubmit={this.sendForm} autoComplete="off">
              <div className="form-group">
                <div className="form-input">
                  <input
                    id="cpf"
                    placeholder="Informe o seu CPF"
                    value={cpf}
                    maxLength="14"
                    onChange={this.handleChange}
                  />
                </div>
                <div className="form-button">
                  <button
                    className="btn btn-green"
                    disabled={!validateCpf(cpf)}
                  >
                    Próximo
                  </button>
                </div>
              </div>
            </form>
          </section>
        </section>
      </div>
    );
  }
}

export default withRouter(
  connect(
    (state) => {
      return {
        cpf: state.loans.cpf,
      };
    },
    (dispatch) => {
      return {
        addCpf: (object, callback) => {
          dispatch(addCpf(object, callback));
        },
      };
    }
  )(Home)
);
